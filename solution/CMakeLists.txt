file(GLOB_RECURSE sources CONFIGURE_DEPENDS
    src/*.c
    src/*.h
    src/*.s
    include/*.h
)

add_executable(image-transformer ${sources})
