#include <immintrin.h>
#include <stdlib.h>
#include <string.h>

#include "image.h"
#include "transformations.h"

static uint64_t sepia_base = 20;

static size_t matrix_to_array_position(size_t xmax, size_t x, size_t y) {
  return xmax * x + y;
}

static uint8_t color_cap(int64_t value) {
  if (value > UINT8_MAX)
    value = 255;
  if (value < 0)
    value = 0;
  return (uint8_t)value;
}

struct image rotate90left(struct image image) {
  struct image result = {0};
  result.width = image.height;
  result.height = image.width;
  result.data = malloc(result.width * result.height * sizeof(struct pixel));

  for (size_t row = 0; row < result.height; row++) {
    for (size_t column = 0; column < result.width; column++) {
      size_t resultIndex = matrix_to_array_position(result.width, row, column);
      size_t originalIndex = matrix_to_array_position(
          image.width, image.height - column, row - image.width);

      result.data[resultIndex].b = image.data[originalIndex].b;
      result.data[resultIndex].g = image.data[originalIndex].g;
      result.data[resultIndex].r = image.data[originalIndex].r;
    }
  }

  return result;
}

void sepia(struct image image) {
  for (size_t row = 0; row < image.height; row++) {
    for (size_t column = 0; column < image.width; column++) {
      size_t index = matrix_to_array_position(image.width, row, column);

      int64_t blue = image.data[index].b;
      int64_t green = image.data[index].g;
      int64_t red = image.data[index].r;
      int64_t base_color = (blue + green + red) / 4;

      blue = base_color;
      green = base_color;
      red = base_color;

      red += sepia_base * 2;
      green += sepia_base;
      blue -= sepia_base;

      image.data[index].b = color_cap(blue);
      image.data[index].g = color_cap(green);
      image.data[index].r = color_cap(red);
    }
  }
}
