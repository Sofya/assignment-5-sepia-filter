#include <stdbool.h>
#include <stdio.h>

#include "image.h"

bool to_bmp(FILE *file, struct image *image);
bool from_bmp(FILE *file, struct image *image);

