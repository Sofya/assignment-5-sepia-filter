#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>

#include "bmp.h"
#include "file.h"
#include "image.h"
#include "transformations.h"

int main(int argc, char **argv) {
  if (argc != 4) {
    puts("Syntax failure");
    return 1;
  }

  intmax_t mode = strtoimax(argv[1], NULL, 10);
  if (!(mode == 0 || mode == 1)) {
    puts("Operation unrecognized");
    return 1;
  }

  FILE *file_in = fopen(argv[2], "rb");
  if (!file_in) {
    puts("Input file access error");
    return 1;
  }

  FILE *file_out = fopen(argv[3], "wb");
  if (!file_out) {
    puts("Output file access error");
    return 1;
  }

  struct image image = {0};
  if (!from_bmp(file_in, &image)) {
    puts("File read error");
    return 1;
  }

  if (mode == 0) {
    sepia(image);
  } else if (mode == 1) {
    sepia_simd(image);
  }

  if (!to_bmp(file_out, &image)) {
    puts("File write error");
    return 1;
  }

  free(image.data);
  fclose(file_in);
  fclose(file_out);
  return 0;
}
