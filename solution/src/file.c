#include <stdlib.h>

#include "bmp.h"
#include "file.h"

bool to_bmp(FILE *file, struct image *image) {
  unsigned char zeros[64] = {0};
  if (!file || !image) {
    return false;
  }

  uint64_t bitmapSize =
      ((sizeof(struct pixel) * image->width) + get_bmp_padding(image->width)) *
      image->height;
  uint64_t fileSize = sizeof(struct bmp_header) + bitmapSize;

  if (bitmapSize >= INT32_MAX || fileSize >= INT32_MAX ||
      image->width >= INT32_MAX || image->height >= INT32_MAX) {
    return false;
  }

  long int pos_to_restore = ftell(file);

  struct bmp_header header = {0};
  header.bfType = (uint16_t)0x4D42;
  header.bfileSize = (uint32_t)fileSize;
  header.bOffBits = sizeof(struct bmp_header);
  header.biSize = 40;
  header.biWidth = (uint32_t)image->width;
  header.biHeight = (uint32_t)image->height;
  header.biPlanes = 1;
  header.biBitCount = 24;
  header.biSizeImage = (uint32_t)bitmapSize;

  if (fwrite(&header, sizeof(struct bmp_header), 1, file) != 1) {
    return false;
  }

  for (size_t row = 0; row < image->height; row++) {
    if (fwrite(image->data + (row * image->width), sizeof(struct pixel),
               image->width, file) != image->width)
      return false;
    if (fwrite(&zeros, get_bmp_padding(image->width), 1, file) != 1) {
      return false;
    }
  }

  fseek(file, pos_to_restore, SEEK_SET);
  return true;
}

bool from_bmp(FILE *file, struct image *image) {
  if (!file || !image) {
    return false;
  }

  long int pos_to_restore = ftell(file);
  struct bmp_header header;
  struct pixel pixel;

  fseek(file, 0, SEEK_SET);
  if (fread(&header, sizeof(struct bmp_header), 1, file) != 1) {
    fseek(file, pos_to_restore, SEEK_SET);
    return false;
  }

  if (header.bfType != (uint16_t)0x4D42) {
    fseek(file, pos_to_restore, SEEK_SET);
    return false;
  }

  image->height = header.biHeight;
  image->width = header.biWidth;
  image->data =
      malloc(image->height * image->width * sizeof(struct pixel) + 32);

  for (size_t row = 0; row < image->height; row++) {
    for (size_t column = 0; column < image->width; column++) {
      if (fread(&pixel, sizeof(struct pixel), 1, file) != 1) {
        free(image->data);
        image->height = 0;
        image->width = 0;
        image->data = NULL;
        fseek(file, pos_to_restore, SEEK_SET);
        return false;
      }
      image->data[row * image->width + column] = pixel;
    }
    if (fread(&pixel, get_bmp_padding(image->width), 1, file) != 1) {
      free(image->data);
      image->height = 0;
      image->width = 0;
      image->data = NULL;
      fseek(file, pos_to_restore, SEEK_SET);
      return false;
    }
  }

  fseek(file, pos_to_restore, SEEK_SET);
  return true;
}

