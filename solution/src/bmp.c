#include "bmp.h"

size_t get_bmp_padding(size_t width) { return width % 4; }
