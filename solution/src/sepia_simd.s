.section .data

.align 16
.sepia_value:
.value	20
.value	20
.value	20
.value	20
.value	20
.value	20
.value	20
.value	20

.align 16
.sepia_ceil:
.value	255
.value	255
.value	255
.value	255
.value	255
.value	255
.value	255
.value	255

.section .text
.global	sepia_simd

sepia_simd:
	pushq	%rbp	
  pushq	%rbx	

  #load consts
	movdqa	.sepia_value(%rip), %xmm15	
	movdqa	.sepia_ceil(%rip), %xmm14	

  # get the pixel count
	movq	24(%rsp), %r9	
	imulq	32(%rsp), %r9	

  # retrieve bitmap array address
	movq	40(%rsp), %rax	

  # reset pixel counter
  xorl %r8d, %r8d

.loop:
	cmpq	%r8, %r9	# done yet?
	jbe	.loop_done	

	addq	$8, %r8 # move pixel counter
	addq	$24, %rax	# move bitmap pointer
  
#clear
  pxor %xmm0, %xmm0
  pxor %xmm1, %xmm1
  pxor %xmm2, %xmm2

#load
#blue
	pinsrb	$0,  -24(%rax),	%xmm0
	pinsrb	$2,  -21(%rax),	%xmm0
	pinsrb	$4,  -18(%rax),	%xmm0
	pinsrb	$6,  -15(%rax),	%xmm0
	pinsrb	$8,  -12(%rax),	%xmm0
	pinsrb	$10,  -9(%rax), %xmm0
	pinsrb	$12,  -6(%rax), %xmm0
	pinsrb	$14,  -3(%rax), %xmm0

#green
	pinsrb	$0,  -23(%rax),	%xmm1
	pinsrb	$2,  -20(%rax),	%xmm1
	pinsrb	$4,  -17(%rax),	%xmm1
	pinsrb	$6,  -14(%rax),	%xmm1
	pinsrb	$8,  -11(%rax),	%xmm1
	pinsrb	$10,  -8(%rax), %xmm1
	pinsrb	$12,  -5(%rax), %xmm1
	pinsrb	$14,  -2(%rax), %xmm1

#red
	pinsrb	$0,  -22(%rax), %xmm2	
	pinsrb	$2,  -19(%rax), %xmm2	
	pinsrb	$4,  -16(%rax), %xmm2	
	pinsrb	$6,  -13(%rax), %xmm2	
	pinsrb	$8,  -10(%rax), %xmm2	
	pinsrb	$10,  -7(%rax), %xmm2	
	pinsrb	$12,  -4(%rax), %xmm2	
	pinsrb	$14,  -1(%rax), %xmm2	

  # calculations
  # sum them and divide by 4
	paddw	%xmm1, %xmm0	
	paddw	%xmm2, %xmm0	
	psrlw	$2, %xmm0

  # green
	movdqa	%xmm0, %xmm1	
	paddw	%xmm15, %xmm1	

  # red
	movdqa	%xmm0, %xmm2	
	paddw	%xmm15, %xmm2	
	paddw	%xmm15, %xmm2	

  # use max to not get into negatives
  #blue
	pmaxsw	%xmm15, %xmm0	
	psubw	%xmm15, %xmm0	

  #compact into 8bits
	pminsw	%xmm14, %xmm0
	pminsw	%xmm14, %xmm1
	pminsw	%xmm14, %xmm2

#store
#blue
	pextrb	$0, %xmm0, -24(%rax)	
	pextrb	$2, %xmm0, -21(%rax)	
	pextrb	$4, %xmm0, -18(%rax)	
	pextrb	$6, %xmm0, -15(%rax)	
	pextrb	$8, %xmm0, -12(%rax)	
	pextrb	$10, %xmm0, -9(%rax)	
	pextrb	$12, %xmm0, -6(%rax)	
	pextrb	$14, %xmm0, -3(%rax)	

#green
	pextrb	$0, %xmm1, -23(%rax)	
	pextrb	$2, %xmm1, -20(%rax)	
	pextrb	$4, %xmm1, -17(%rax)	
	pextrb	$6, %xmm1, -14(%rax)	
	pextrb	$8, %xmm1, -11(%rax)	
	pextrb	$10, %xmm1, -8(%rax)	
	pextrb	$12, %xmm1, -5(%rax)	
	pextrb	$14, %xmm1, -2(%rax)	

#red
	pextrb	$0, %xmm2, -22(%rax)	
	pextrb	$2, %xmm2, -19(%rax)	
	pextrb	$4, %xmm2, -16(%rax)	
	pextrb	$6, %xmm2, -13(%rax)	
	pextrb	$8, %xmm2, -10(%rax)	
	pextrb	$10, %xmm2, -7(%rax)	
	pextrb	$12, %xmm2, -4(%rax)	
	pextrb	$14, %xmm2, -1(%rax)	

	jmp	.loop	

.loop_done:
	popq	%rbx
	popq	%rbp
	ret	

