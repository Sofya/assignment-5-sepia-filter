rm -rf output1.bmp output2.bmp build
cmake -S ./ -B ./build
cd ./build
make
cd ..

echo "C solution:"
time ./build/solution/image-transformer 0 input.bmp output1.bmp
echo "Asm solution:"
time ./build/solution/image-transformer 1 input.bmp output2.bmp
